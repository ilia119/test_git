package by.igoroshko;

import com.google.common.collect.Sets;
import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class test implements Comparable {

    public static void main(String[] args) {
        List<Set<Integer>> list = new ArrayList<>();
        Set<Integer> s1 = new HashSet<>();
        s1.add(1);
        s1.add(2);
        s1.add(3);
        Set<Integer> s2 = new HashSet<>();
        s2.add(1);
        s2.add(2);
        s2.add(4);
        Set<Integer> s3 = new HashSet<>();
        s3.add(1);
        s3.add(2);
        s3.add(5);
        s3.add(6);
        Set<Integer> s4 = new HashSet<>();
        s4.add(1);
        s4.add(2);
        s4.add(5);
        s4.add(6);
        list.add(s1);
        list.add(s2);
        list.add(s3);
        list.add(s4);
        list.add(s4);

        Set<Integer> result = list.get(0);
        list.forEach(result::retainAll);

        System.out.println(result);
    Set<Integer> reduce = list
            .stream()
            .reduce(
                    null,
                    (eventSet1, eventSet2) -> {
                        if (eventSet1 == null) {
                            return eventSet2;
                        } else {
                            return Sets.intersection(eventSet1, eventSet2);
                        }
                    }
            );

        System.out.println(reduce);

    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }


/*
    Set<Integer> reduce = list
            .stream()
            .reduce(
                    null,
                    (eventSet1, eventSet2) -> {
                        if (eventSet1 == null) {
                            return eventSet2;
                        } else {
                            return Sets.intersection(eventSet1, eventSet2);
                        }
                    }
            );
*/
}
